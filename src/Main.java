import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

import dataStructures.Entry;
import dataStructures.Iterator;
import fitnessTracker.AthleteGets;
import fitnessTracker.DefaultFitnessTracker;
import fitnessTracker.FitnessTracker;
import fitnessTracker.GroupGets;
import fitnessTracker.Training;
import fitnessTracker.exceptions.AthleteAlreadyInGroupException;
import fitnessTracker.exceptions.AthleteNotInGroupException;
import fitnessTracker.exceptions.EmptyContestException;
import fitnessTracker.exceptions.ExistingActivityException;
import fitnessTracker.exceptions.ExistingAthleteException;
import fitnessTracker.exceptions.ExistingGroupException;
import fitnessTracker.exceptions.GroupIsEmptyException;
import fitnessTracker.exceptions.InexistentActivityException;
import fitnessTracker.exceptions.InexistentAthleteException;
import fitnessTracker.exceptions.InexistentAthleteTrainingsException;
import fitnessTracker.exceptions.InexistentGroupException;
import fitnessTracker.exceptions.InvalidArgumentQuantityException;
import fitnessTracker.exceptions.InvalidListingTypeException;
import fitnessTracker.exceptions.InvalidSexException;

/**
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public class Main {

	/**
	 * Commands
	 */
	private static final String INSERT_ATHLETE = "IU";
	private static final String EDIT_ATHLETE = "UU";
	private static final String REMOVE_ATHLETE = "RU";
	private static final String CONSULT_ATHLETE = "CU";
	private static final String NEW_ACTIVITY = "IA";
	private static final String NEW_TRAINING = "AW";
	private static final String CONSULT_TRAININGS = "CW";
	private static final String ADD_STEPS = "AS";
	private static final String NEW_GROUP = "IG";
	private static final String JOIN_GROUP = "AG";
	private static final String QUIT_GROUP = "DG";
	private static final String CONSULT_GROUP = "CG";
	private static final String LIST_ATHLETES_IN_GROUP = "LG";
	private static final String LIST_WALKERS = "LC";
	private static final String LIST_WARRIORS = "LW";
	private static final String EXIT_COMMAND = "XS";

	/**
	 * Commands number of parameters
	 */
	private static final int P_INSERT_ATHLETE = 7;
	private static final int P_EDIT_ATHLETE = 4;
	private static final int P_REMOVE_ATHLETE = 1;
	private static final int P_CONSULT_ATHLETE = 1;
	private static final int P_NEW_ACTIVITY = 3;
	private static final int P_NEW_TRAINING = 3;
	private static final int P_CONSULT_TRAININGS = 2;
	private static final int P_ADD_STEPS = 2;
	private static final int P_NEW_GROUP = 2;
	private static final int P_JOIN_GROUP = 2;
	private static final int P_QUIT_GROUP = 2;
	private static final int P_CONSULT_GROUP = 1;
	private static final int P_LIST_ATHLETES_IN_GROUP = 1;
	private static final int P_LIST_WALKERS = 0;
	private static final int P_LIST_WARRIORS = 0;

	/**
	 * Error messages
	 */
	private static final String INVALID_VALUES = "Valores invalidos.\n";
	private static final String EXISTENT_ATHLETE = "Atleta existente.\n";
	private static final String INEXISTENT_ATHLETE = "Atleta inexistente.\n";
	private static final String EXISTENT_ACTIVITY = "Atividade existente.\n";
	private static final String INEXISTENT_ACTIVITY = "Atividade inexistente.\n";
	private static final String INVALID_MET = "MET invalido.\n";
	private static final String INVALID_TIME = "Tempo invalido.\n";
	private static final String INVALID_OPTION = "Opcao invalida.\n";
	private static final String INEXISTENT_TRAININGS = "Atleta sem treinos.\n";
	private static final String INVALID_STEPS = "Numero de passos invalido.\n";
	private static final String EXISTENT_GROUP = "Grupo existente.\n";
	private static final String INEXISTENT_GROUP = "Grupo inexistente.\n";
	private static final String ATHLETE_ALREADY_IN_GROUP = "Atleta ja tem grupo.\n";
	private static final String ATHLETE_NOT_IN_GROUP = "Atleta nao pertence ao grupo.\n";
	private static final String GROUP_IS_EMPTY = "Grupo nao tem adesoes.\n";
	private static final String NO_GROUPS_EXIST = "Nao existem grupos.\n";
	private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments.\n";
	private static final String SAVE_ERROR_MSG = "Couldn't write the program state";

	/**
	 * Success messages
	 */
	private static final String INSERT_ATHLETE_SUCESS = "Insercao de atleta com sucesso.\n";
	private static final String EDIT_ATHLETE_SUCESS = "Atleta atualizado com sucesso.\n";
	private static final String REMOVE_ATHLETE_SUCESS = "Atleta removido com sucesso.\n";
	private static final String NEW_ACTIVITY_SUCESS = "Atividade criada com sucesso.\n";
	private static final String NEW_TRAINING_SUCESS = "Treino adicionado com sucesso.\n";
	private static final String UPDATE_STEPS_SUCESS = "Atualizacao de passos com sucesso.\n";
	private static final String NEW_GROUP_SUCESS = "Grupo criado com sucesso.\n";
	private static final String JOIN_GROUP_SUCESS = "Adesao realizada com sucesso.\n";
	private static final String QUIT_GROUP_SUCESS = "Desistencia realizada com sucesso.\n";
	private static final String EXIT_SUCESS = "Gravando e terminando...\n";

	/**
	 * Utilities
	 */
	private static final String FILE_NAME = "data.dat";
	private static final String COMMAND_READER_SPLIT = " ";
	private static final String NAME_SEPARATOR = " ";
	private static final String EMPTY_STRING = "";
	private static final int STRING_START_POSITION = 0;
	private static final int COMMAND_NAME_POSITION = 0;
	private static final int ARRAY_LENGTH_CORRECTOR = 1;

	private static FitnessTracker tracker;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		tracker = load();
		menuSwitch(in);
		in.close();
	}

	/**
	 * Returns an array of strings. Where: out[0] is the command out[...] are
	 * the parameters for the command
	 * 
	 * @param input
	 * @return parameters array
	 */
	private static String[] commandReader(Scanner in) {
		String command = in.nextLine();
		String[] out = command.split(COMMAND_READER_SPLIT);
		out[COMMAND_NAME_POSITION] = out[COMMAND_NAME_POSITION].toUpperCase();
		return out;
	}

	private static void menuSwitch(Scanner in) {
		String command[] = { EMPTY_STRING };
		try {
			while (!command[COMMAND_NAME_POSITION].equals(EXIT_COMMAND)) {
				command = commandReader(in);
				switch (command[0]) {
				case INSERT_ATHLETE:
					insertAthlete(command);
					break;
				case EDIT_ATHLETE:
					editAthlete(command);
					break;
				case REMOVE_ATHLETE:
					removeAthlete(command);
					break;
				case CONSULT_ATHLETE:
					consultAthlete(command);
					break;
				case NEW_ACTIVITY:
					newActivity(command);
					break;
				case NEW_TRAINING:
					newTraining(command);
					break;
				case CONSULT_TRAININGS:
					listAthleteTrainings(command);
					break;
				case ADD_STEPS:
					updateSteps(command);
					break;
				case NEW_GROUP:
					newGroup(command);
					break;
				case JOIN_GROUP:
					joinGroup(command);
					break;
				case QUIT_GROUP:
					quitGroup(command);
					break;
				case CONSULT_GROUP:
					consultGroup(command);
					break;
				case LIST_ATHLETES_IN_GROUP:
					listGroupAthletes(command);
					break;
				case LIST_WALKERS:
					listWalkers(command);
					break;
				case LIST_WARRIORS:
					listWarriors(command);
					break;
				default:
					break;
				}
			}
			exit();
		} catch (InvalidArgumentQuantityException e) {
			System.out.println(INVALID_NUMBER_OF_ARGUMENTS);
			menuSwitch(in);
		}
	}

	private static void commandArgumentChecker(int argumentCount, int expectedArgumentCount)
			throws InvalidArgumentQuantityException {
		int argC = argumentCount - ARRAY_LENGTH_CORRECTOR;

		if (argC < expectedArgumentCount) {
			throw new InvalidArgumentQuantityException();
		}

	}

	private static String getFullName(String[] stringArray, int subset) {
		String result = EMPTY_STRING;
		for (int i = stringArray.length - subset; i < stringArray.length; i++) {
			if (i != stringArray.length - ARRAY_LENGTH_CORRECTOR) {
				result += stringArray[i] + NAME_SEPARATOR;
			} else {
				result += stringArray[i];
			}
		}
		return result;
	}

	private static void insertAthlete(String[] command) throws InvalidArgumentQuantityException {
		commandArgumentChecker(command.length, P_INSERT_ATHLETE);
		try {
			String trackerID = command[1].toLowerCase();
			int weight = Integer.parseInt(command[2]);
			int height = Integer.parseInt(command[3]);
			int age = Integer.parseInt(command[4]);
			char sex = command[5].toUpperCase().charAt(STRING_START_POSITION);
			String name = getFullName(command, command.length - 6);
			tracker.insertAthlete(trackerID, weight, height, age, sex, name);
			System.out.println(INSERT_ATHLETE_SUCESS);
		} catch (NumberFormatException e) {
			System.out.println(INVALID_VALUES);
		} catch (ExistingAthleteException e) {
			System.out.println(EXISTENT_ATHLETE);
		} catch (InvalidSexException e) {
			System.out.println(INVALID_VALUES);
		}
	}

	private static void editAthlete(String[] command) throws InvalidArgumentQuantityException {
		commandArgumentChecker(command.length, P_EDIT_ATHLETE);
		try {
			String trackerID = command[1].toLowerCase();
			int weight = Integer.parseInt(command[2]);
			int height = Integer.parseInt(command[3]);
			int age = Integer.parseInt(command[4]);
			tracker.editAthleteInfo(trackerID, weight, height, age);
			System.out.println(EDIT_ATHLETE_SUCESS);
		} catch (InexistentAthleteException e) {
			System.out.println(INEXISTENT_ATHLETE);
		} catch (NumberFormatException e) {
			System.out.println(INVALID_VALUES);
		}
	}

	private static void removeAthlete(String[] command) throws InvalidArgumentQuantityException {
		commandArgumentChecker(command.length, P_REMOVE_ATHLETE);
		try {
			String trackerID = command[1].toLowerCase();
			tracker.removeAthlete(trackerID);
			System.out.println(REMOVE_ATHLETE_SUCESS);
		} catch (InexistentAthleteException e) {
			System.out.println(INEXISTENT_ATHLETE);
		} catch (AthleteNotInGroupException e) {
			//This exception will never be thrown
			System.out.println(ATHLETE_NOT_IN_GROUP);
		} catch (InexistentGroupException e) {
			//This exception will never be thrown
			System.out.println(INEXISTENT_GROUP);
		}
	}

	private static void consultAthlete(String[] command) throws InvalidArgumentQuantityException {
		commandArgumentChecker(command.length, P_CONSULT_ATHLETE);
		try {
			String trackerID = command[1].toLowerCase();
			System.out.println(tracker.getAthlete(trackerID));

			System.out.println(EMPTY_STRING);
		} catch (InexistentAthleteException e) {
			System.out.println(INEXISTENT_ATHLETE);
		}
	}

	private static void newActivity(String[] command) throws InvalidArgumentQuantityException {
		commandArgumentChecker(command.length, P_NEW_ACTIVITY);
		try {
			String activityID = command[1].toLowerCase();
			int MET = Integer.parseInt(command[2]);
			String name = getFullName(command, command.length - 3);
			tracker.newActivity(activityID, MET, name);
			System.out.println(NEW_ACTIVITY_SUCESS);
		} catch (NumberFormatException e) {
			System.out.println(INVALID_MET);
		} catch (ExistingActivityException e) {
			System.out.println(EXISTENT_ACTIVITY);
		}
	}

	private static void newTraining(String[] command) throws InvalidArgumentQuantityException {
		commandArgumentChecker(command.length, P_NEW_TRAINING);
		try {
			String trackerID = command[1].toLowerCase();
			String activityID = command[2].toLowerCase();
			int duration = Integer.parseInt(command[3]);
			tracker.newTraining(trackerID, activityID, duration);
			System.out.println(NEW_TRAINING_SUCESS);
		} catch (NumberFormatException e) {
			System.out.println(INVALID_TIME);
		} catch (InexistentActivityException e) {
			System.out.println(INEXISTENT_ACTIVITY);
		} catch (InexistentAthleteException e) {
			System.out.println(INEXISTENT_ATHLETE);
		} catch (InexistentGroupException e) {
			//This exception will never be thrown
			System.out.println(INEXISTENT_GROUP);
		} catch (AthleteNotInGroupException e) {
			//This exception will never be thrown
			System.out.println(ATHLETE_NOT_IN_GROUP);
		}
	}

	private static void listAthleteTrainings(String[] command) throws InvalidArgumentQuantityException {
		commandArgumentChecker(command.length, P_CONSULT_TRAININGS);
		try {
			String trackerID = command[1].toLowerCase();
			char listType = command[2].toUpperCase().charAt(STRING_START_POSITION);
			Iterator<Training> it = tracker.listAthleteTrainings(trackerID, listType);
			it.rewind();

			while (it.hasNext()) {
				System.out.println(it.next());
			}
			// Prints an empty line after listing
			System.out.println(EMPTY_STRING);
		} catch (InvalidListingTypeException e) {
			System.out.println(INVALID_OPTION);
		} catch (InexistentAthleteException e) {
			System.out.println(INEXISTENT_ATHLETE);
		} catch (InexistentAthleteTrainingsException e) {
			System.out.println(INEXISTENT_TRAININGS);
		}
	}

	private static void updateSteps(String[] command) throws InvalidArgumentQuantityException {
		commandArgumentChecker(command.length, P_ADD_STEPS);
		try {
			String trackerID = command[1].toLowerCase();
			int steps = Integer.parseInt(command[2]);
			tracker.updateSteps(trackerID, steps);
			System.out.println(UPDATE_STEPS_SUCESS);
		} catch (NumberFormatException e) {
			System.out.println(INVALID_STEPS);
		} catch (InexistentAthleteException e) {
			System.out.println(INEXISTENT_ATHLETE);
		} catch (InexistentGroupException e) {
			//This exception will never be thrown
			System.out.println(INEXISTENT_GROUP);
		} catch (AthleteNotInGroupException e) {
			//This exception will never be thrown
			System.out.println(ATHLETE_NOT_IN_GROUP);
		}
	}

	private static void newGroup(String[] command) throws InvalidArgumentQuantityException {
		commandArgumentChecker(command.length, P_NEW_GROUP);
		try {
			String groupID = command[1].toLowerCase();
			String name = getFullName(command, command.length - 2);
			tracker.newGroup(groupID, name);
			System.out.println(NEW_GROUP_SUCESS);
		} catch (ExistingGroupException e) {
			System.out.println(EXISTENT_GROUP);
		} catch (InexistentGroupException e) {
			System.out.println("Couldnt add group to contest");
		}
	}

	private static void joinGroup(String[] command) throws InvalidArgumentQuantityException {
		commandArgumentChecker(command.length, P_JOIN_GROUP);
		try {
			String trackerID = command[1].toLowerCase();
			String groupID = command[2].toLowerCase();
			tracker.joinGroup(trackerID, groupID);
			System.out.println(JOIN_GROUP_SUCESS);
		} catch (InexistentAthleteException e) {
			System.out.println(INEXISTENT_ATHLETE);
		} catch (InexistentGroupException e) {
			System.out.println(INEXISTENT_GROUP);
		} catch (AthleteAlreadyInGroupException e) {
			System.out.println(ATHLETE_ALREADY_IN_GROUP);
		}
	}

	private static void quitGroup(String[] command) throws InvalidArgumentQuantityException {
		commandArgumentChecker(command.length, P_QUIT_GROUP);
		try {
			String trackerID = command[1].toLowerCase();
			String groupID = command[2].toLowerCase();
			tracker.quitGroup(trackerID, groupID);
			System.out.println(QUIT_GROUP_SUCESS);
		} catch (InexistentAthleteException e) {
			System.out.println(INEXISTENT_ATHLETE);
		} catch (InexistentGroupException e) {
			System.out.println(INEXISTENT_GROUP);
		} catch (AthleteNotInGroupException e) {
			System.out.println(ATHLETE_NOT_IN_GROUP);
		}
	}

	private static void consultGroup(String[] command) throws InvalidArgumentQuantityException {
		commandArgumentChecker(command.length, P_CONSULT_GROUP);
		try {
			String groupID = command[1].toLowerCase();
			System.out.println(tracker.consultGroupStats(groupID));
			// Prints an empty line after the output
			System.out.println(EMPTY_STRING);
		} catch (InexistentGroupException e) {
			System.out.println(INEXISTENT_GROUP);
		}
	}

	private static void listGroupAthletes(String[] command) throws InvalidArgumentQuantityException {
		commandArgumentChecker(command.length, P_LIST_ATHLETES_IN_GROUP);
		try {
			String groupID = command[1].toLowerCase();
			Iterator<Entry<String, AthleteGets>> it;
			it = tracker.listAthletesInGroup(groupID);
			while (it.hasNext()) {
				System.out.println(it.next().getValue().athletesInGroupListFormat());
			}
			// Prints an empty line after listing
			System.out.println(EMPTY_STRING);
		} catch (InexistentGroupException e) {
			System.out.println(INEXISTENT_GROUP);
		} catch (GroupIsEmptyException e) {
			System.out.println(GROUP_IS_EMPTY);
		}
	}

	private static void listWalkers(String[] command) throws InvalidArgumentQuantityException {
		commandArgumentChecker(command.length, P_LIST_WALKERS);
		try {
			Iterator<GroupGets> it;
			it = tracker.listGroupsWalkersContest();
			while (it.hasNext()) {
				System.out.println(it.next());
			}
			// Prints an empty line after listing
			System.out.println(EMPTY_STRING);
		} catch (EmptyContestException e) {
			System.out.println(NO_GROUPS_EXIST);
		}
	}

	private static void listWarriors(String[] command) throws InvalidArgumentQuantityException {
		commandArgumentChecker(command.length, P_LIST_WARRIORS);
		try {
			Iterator<GroupGets> it;
			it = tracker.listGroupsWarriorsContest();
			while (it.hasNext()) {
				System.out.println(it.next());
			}
			// Prints an empty line after listing
			System.out.println(EMPTY_STRING);
		} catch (EmptyContestException e) {
			System.out.println(NO_GROUPS_EXIST);
		}
	}

	private static void exit() {
		save();
		System.out.println(EXIT_SUCESS);
		System.exit(0);
	}

	private static void save() {
		try {
			ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(FILE_NAME));
			outputStream.writeObject(tracker);
			outputStream.flush();
			outputStream.close();
		} catch (IOException e) {
			System.out.println(SAVE_ERROR_MSG);
		}
	}

	private static FitnessTracker load() {
		try {
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(FILE_NAME));
			FitnessTracker loadedTracker = (FitnessTracker) inputStream.readObject();
			inputStream.close();
			return loadedTracker;
		} catch (IOException e) {
			return new DefaultFitnessTracker();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
}
