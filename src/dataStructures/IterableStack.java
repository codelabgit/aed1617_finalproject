package dataStructures;

/**
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public interface IterableStack<E> extends Stack<E> {

	/**
	 * Returns an iterator for the elements in the stack
	 * 
	 * @return iterator
	 */
	Iterator<E> iterator();
}
