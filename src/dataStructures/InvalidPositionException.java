package dataStructures;

public class InvalidPositionException extends Exception {

	private static final long serialVersionUID = 0L;

	public InvalidPositionException() {
		super();
	}

	public InvalidPositionException(String message) {
		super(message);
	}
}
