package dataStructures;

/**
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public class IterableStackInList<E> extends StackInList<E> implements IterableStack<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Iterator<E> iterator() {
		return list.iterator();
	}

}
