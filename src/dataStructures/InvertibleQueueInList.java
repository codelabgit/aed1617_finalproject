package dataStructures;

public class InvertibleQueueInList<E> extends QueueInList<E> implements InvertibleQueue<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 0L;
	private boolean isInverted = false;

	@Override
	public void enqueue(E element) {
		if (isInverted) {
			list.addFirst(element);
		} else {
			list.addLast(element);
		}
	}

	@Override
	public E dequeue() throws EmptyQueueException {
		if (list.isEmpty()) {
			throw new EmptyQueueException("Queue is empty.");
		}
		if (isInverted) {
			return list.removeLast();
		} else {
			return list.removeFirst();
		}
	}

	@Override
	public void invert() {
		isInverted = true;
	}

}
