package dataStructures;

/**
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public class ChainedHashTableIterator<K, V> implements Iterator<Entry<K, V>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Iterator<Entry<K, V>> currentIterator;

	private Dictionary<K, V>[] table;

	private int current;

	public ChainedHashTableIterator(Dictionary<K, V>[] table) {
		this.table = table;
		currentIterator = null;
		this.rewind();
	}

	@Override
	public boolean hasNext() {
		if (currentIterator == null)
			return false;
		else
			return currentIterator.hasNext();

	}

	@Override
	public Entry<K, V> next() throws NoSuchElementException {
		if (!hasNext())
			throw new NoSuchElementException();

		Entry<K, V> entry = currentIterator.next();

		if (!hasNext())
			nextIterator(++current);

		return entry;
	}

	@Override
	public void rewind() {
		current = 0;
		nextIterator(current);
	}

	private void nextIterator(int start) {
		for (int i = start; i < table.length; i++) {
			if (!table[i].isEmpty()) {
				currentIterator = table[i].iterator();
				current = i;
				break;
			}
		}
	}

}
