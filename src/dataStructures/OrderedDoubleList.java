package dataStructures;

/**
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public class OrderedDoubleList<K extends Comparable<K>, V> implements OrderedDictionary<K, V> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private DListNode<Entry<K, V>> head;
	private DListNode<Entry<K, V>> tail;

	private int size;

	public OrderedDoubleList() {
		head = null;
		tail = null;
		size = 0;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public V find(K key) {
		DListNode<Entry<K, V>> node = head;
		while (node != null && !node.getElement().getKey().equals(key)) {
			node = node.getNext();
		}
		if (node == null) {
			return null;
		} else {
			return node.getElement().getValue();
		}
	}

	@Override
	public V insert(K key, V value) {

		Entry<K, V> newEntry = new EntryClass<K, V>(key, value);
		if (this.isEmpty()) {
			addEntryFirst(newEntry);
			return null;
		}

		DListNode<Entry<K, V>> node = findNode(key);
		if (node != null) {
			Entry<K, V> entry = node.getElement();

			if (entry.getKey().compareTo(key) == 0) {
				V retValue = entry.getValue();
				node.setElement(newEntry);
				return retValue;
			} else if (entry.getKey().compareTo(key) > 0) {
				if (node == head) {
					addEntryFirst(newEntry);
				} else {
					addEntryBetween(newEntry, node);
				}
				return null;
			}
		}
		addEntryLast(newEntry);
		return null;
	}

	@Override
	public V remove(K key) {
		if (this.isEmpty()) {
			return null;
		}

		DListNode<Entry<K, V>> node = findNode(key);

		if (node != null) {
			Entry<K, V> entry = node.getElement();
			if (entry.getKey().compareTo(key) == 0) {
				V retValue = entry.getValue();
				if (node == head) {
					removeFirstNode();
				} else if (node == tail) {
					removeLastNode();
				} else {
					removeNodeBetween(node);
				}
				return retValue;
			}
		}
		return null;
	}

	@Override
	public Iterator<Entry<K, V>> iterator() {
		return new DoublyLLIterator<Entry<K, V>>(head, tail);
	}

	@Override
	public Entry<K, V> minEntry() throws EmptyDictionaryException {
		return head.getElement();
	}

	@Override
	public Entry<K, V> maxEntry() throws EmptyDictionaryException {
		return tail.getElement();
	}

	/**
	 * Searches for the node corresponding to the Entry of the given key.
	 * 
	 * @param key
	 * @return node
	 */
	protected DListNode<Entry<K, V>> findNode(K key) {
		DListNode<Entry<K, V>> node = head;
		while (node != null && node.getElement().getKey().compareTo(key) < 0) {
			node = node.getNext();
		}
		return node;
	}

	/**
	 * Adds a new entry to the head of the list.
	 * 
	 * @param newEntry
	 */
	protected void addEntryFirst(Entry<K, V> newEntry) {
		DListNode<Entry<K, V>> newNode = new DListNode<Entry<K, V>>(newEntry, null, head);
		if (this.isEmpty()) {
			tail = newNode;
		} else {
			head.setPrevious(newNode);
		}
		head = newNode;
		size++;
	}

	/**
	 * Adds a new entry between two nodes. The given node is the node next to
	 * the new entry.
	 * 
	 * @param entry
	 */
	protected void addEntryBetween(Entry<K, V> entry, DListNode<Entry<K, V>> nextNode) {
		DListNode<Entry<K, V>> previousNode = nextNode.getPrevious();
		DListNode<Entry<K, V>> newNode = new DListNode<Entry<K, V>>(entry, previousNode, nextNode);
		previousNode.setNext(newNode);
		nextNode.setPrevious(newNode);
		size++;
	}

	/**
	 * Adds a new entry to the tail of the list.
	 * 
	 * @param newEntry
	 */
	protected void addEntryLast(Entry<K, V> newEntry) {
		DListNode<Entry<K, V>> newNode = new DListNode<Entry<K, V>>(newEntry, tail, null);
		if (this.isEmpty()) {
			head = newNode;
		} else {
			tail.setNext(newNode);
		}
		tail = newNode;
		size++;
	}

	/**
	 * Removes the first node of the list.
	 */
	protected void removeFirstNode() {
		head = head.getNext();
		if (head == null) {
			tail = null;
		} else {
			head.setPrevious(null);
		}
		size--;
	}

	/**
	 * Removes the node before the given one.
	 * 
	 * @param node
	 */
	protected void removeNodeBetween(DListNode<Entry<K, V>> node) {
		DListNode<Entry<K, V>> previousNode = node.getPrevious();
		DListNode<Entry<K, V>> nextNode = node.getNext();

		previousNode.setNext(nextNode);
		nextNode.setPrevious(previousNode);
		size--;
	}

	/**
	 * Removes the last node of the list.
	 */
	protected void removeLastNode() {
		tail = tail.getPrevious();
		if (tail == null) {
			head = null;
		} else {
			tail.setNext(null);
		}
		size--;
	}
}
