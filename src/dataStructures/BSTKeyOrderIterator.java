package dataStructures;

public class BSTKeyOrderIterator<K, V> implements Iterator<Entry<K, V>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected BSTNode<K, V> root;

	protected Stack<BSTNode<K, V>> stack;

	public BSTKeyOrderIterator(BSTNode<K, V> root) {
		this.root = root;
		rewind();
	}

	@Override
	public boolean hasNext() {
		return !stack.isEmpty();
	}

	@Override
	public Entry<K, V> next() throws NoSuchElementException {
		if (!hasNext())
			throw new NoSuchElementException();

		BSTNode<K, V> node = stack.pop();

		if (node.getRight() != null)
			pushToMinimum(node.getRight());

		return node.getEntry();
	}

	@Override
	public void rewind() {
		stack = new StackInList<BSTNode<K, V>>();
		pushToMinimum(root);
	}

	protected void pushToMinimum(BSTNode<K, V> start) {
		BSTNode<K, V> node = start;
		stack.push(node);

		while (node.getLeft() != null) {
			node = node.getLeft();
			stack.push(node);
		}
	}

}
