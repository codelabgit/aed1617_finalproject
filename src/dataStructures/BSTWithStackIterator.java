package dataStructures;

/**
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 * 
 * @param <K>
 * @param <E>
 *
 */
public class BSTWithStackIterator<K, E> implements Iterator<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Iterator for the whole BST
	private BSTKeyOrderIterator<K, IterableStack<E>> treeIterator;

	// Iterator for the stack in the current node
	private Iterator<E> currentIterator;

	public BSTWithStackIterator(BSTKeyOrderIterator<K, IterableStack<E>> treeIterator) {
		this.treeIterator = treeIterator;
		currentIterator = null;
		rewind();
	}

	@Override
	public boolean hasNext() {
		if (currentIterator == null)
			return false;
		else
			return currentIterator.hasNext();
	}

	@Override
	public E next() throws NoSuchElementException {
		if (!this.hasNext())
			throw new NoSuchElementException();

		E element = currentIterator.next();

		// If there are no more elements in this iterator, advance to the next node
		if (!currentIterator.hasNext())
			this.nextIterator();

		return element;
	}

	@Override
	public void rewind() {
		treeIterator.rewind();
		this.nextIterator();

	}

	// This method advances to the stack iterator in the next node
	private void nextIterator() {
		if (!treeIterator.hasNext()) {
			// We reached the end of the tree
			currentIterator = null;
		} else{
			currentIterator = treeIterator.next().getValue().iterator();
		}
	}

}
