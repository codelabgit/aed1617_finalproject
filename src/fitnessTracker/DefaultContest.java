package fitnessTracker;

import dataStructures.AVLTree;
import dataStructures.BSTKeyOrderIterator;
import dataStructures.BSTWithListIterator;
import dataStructures.DoublyLinkedList;
import dataStructures.Iterator;
import dataStructures.List;
import dataStructures.OrderedDictionary;
import fitnessTracker.exceptions.EmptyContestException;

/**
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public class DefaultContest implements Contest {

	private static final long serialVersionUID = 1L;
	private static final int TREE_INVERT = -1;
	private static final int NOT_FOUND = -1;

	private OrderedDictionary<Integer, List<GroupGets>> walkers;
	private OrderedDictionary<Integer, List<GroupGets>> warriors;

	public DefaultContest() {
		walkers = new AVLTree<Integer, List<GroupGets>>();
		warriors = new AVLTree<Integer, List<GroupGets>>();
	}

	@Override
	public void addGroup(GroupGets group) {

		int groupSteps = TREE_INVERT * group.getGroupTotalSteps();
		int groupCalories = TREE_INVERT * group.getGroupTotalSpentCalories();

		List<GroupGets> tempList = walkers.find(groupSteps);

		if (tempList == null) {
			tempList = new DoublyLinkedList<GroupGets>();
			tempList.addLast(group);
			walkers.insert(groupSteps, tempList);
		} else if (tempList.find(group) == NOT_FOUND) {
			tempList.addLast(group);
		}

		tempList = warriors.find(groupCalories);

		if (tempList == null) {
			tempList = new DoublyLinkedList<GroupGets>();
			tempList.addLast(group);
			warriors.insert(groupSteps, tempList);
		} else if (tempList.find(group) == NOT_FOUND) {
			tempList.addLast(group);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public Iterator<GroupGets> getWalkersOfTheYear() throws EmptyContestException {
		if (walkers.isEmpty())
			throw new EmptyContestException();

		BSTKeyOrderIterator<Integer, List<GroupGets>> treeIterator = (BSTKeyOrderIterator<Integer, List<GroupGets>>) walkers
				.iterator();

		return new BSTWithListIterator<Integer, GroupGets>(treeIterator);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Iterator<GroupGets> getWarriorsOfTheYear() throws EmptyContestException {
		if (warriors.isEmpty())
			throw new EmptyContestException();

		BSTKeyOrderIterator<Integer, List<GroupGets>> treeIterator = (BSTKeyOrderIterator<Integer, List<GroupGets>>) warriors
				.iterator();

		return new BSTWithListIterator<Integer, GroupGets>(treeIterator);
	}

	@Override
	public void updateContestStatus(GroupGets lastUpdatedGroup) {
		int groupPreviousSteps = TREE_INVERT * lastUpdatedGroup.getGroupPreviousTotalCalories();
		int groupPreviousCalories = TREE_INVERT * lastUpdatedGroup.getGroupPreviousTotalSteps();

		int groupNewSteps = TREE_INVERT * lastUpdatedGroup.getGroupTotalSteps();
		int groupNewCalories = TREE_INVERT * lastUpdatedGroup.getGroupTotalSpentCalories();
		

		List<GroupGets> warriorsList = warriors.find(groupPreviousCalories);
		List<GroupGets> walkersList = walkers.find(groupPreviousSteps);
		
		
		// Checks if there's any change in the number of calories
		if (groupPreviousCalories != groupNewCalories) {

			if (warriorsList != null) {

				boolean warriorSucess = warriorsList.remove(lastUpdatedGroup);

				if (warriorSucess) {

					// If the list becomes empty after removing the element, the
					// node is removed from the tree
					if (warriorsList.isEmpty()) {
						warriors.remove(groupPreviousCalories);
					}

					// New list with the new number of calories
					List<GroupGets> newWarriorsList = warriors.find(groupNewCalories);

					// If it's empty, create a new one and insert it in the tree
					if (newWarriorsList == null) {
						newWarriorsList = new DoublyLinkedList<GroupGets>();
						warriors.insert(groupNewCalories, newWarriorsList);
					}

					// Add at the tail of the list, preserving the desired order
					newWarriorsList.addLast(lastUpdatedGroup);

					// groupNewCalories is now the previous number of calories
					// next time this method is called
					((GroupSets) lastUpdatedGroup).updateGroupPreviousTotalCalories();
				}
			}
		}
		
		// Checks if there any change in the number of steps
		if (groupPreviousSteps != groupNewSteps) {

			if (walkersList != null) {

				boolean walkerSucess = walkersList.remove(lastUpdatedGroup);

				if (walkerSucess) {
					if (walkersList.isEmpty()) {
						walkers.remove(groupPreviousSteps);
					}

					// New list with the new number of steps
					List<GroupGets> newWalkersList = walkers.find(groupNewSteps);

					if (newWalkersList == null) {
						newWalkersList = new DoublyLinkedList<GroupGets>();
						walkers.insert(groupNewSteps, newWalkersList);
					}

					// groupNewSteps is now the previous number of steps
					// next time this method is called
					newWalkersList.addLast(lastUpdatedGroup);
					((GroupSets) lastUpdatedGroup).updateGroupPreviousTotalSteps();
				}
			}
		}
	}
}
