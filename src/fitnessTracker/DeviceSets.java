package fitnessTracker;

import fitnessTracker.exceptions.AthleteNotInGroupException;

/**
 * Changeable interface. Only for changing the device's data. Can only be used
 * inside its own package.
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
interface DeviceSets extends DeviceGets {
	/**
	 * Adds the number of steps passed as parameter to the device's total step
	 * count.
	 * 
	 * @param steps
	 */
	void updateSteps(int steps) throws AthleteNotInGroupException;

	/**
	 * Inserts a new training session.
	 * 
	 * @param activity
	 * @param trainingDuration
	 */
	void newTraining(Activity activity, int trainingDuration) throws AthleteNotInGroupException;

}
