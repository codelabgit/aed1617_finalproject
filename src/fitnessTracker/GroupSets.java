package fitnessTracker;

/**
 * Changeable interface. Only for changing the device's data. Can only be used
 * inside its own package.
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
interface GroupSets extends GroupGets {
	/**
	 * Updates the total number of steps adding the number passed as parameter
	 * to the total
	 * 
	 * @pre numSteps > 0
	 * @param numSteps
	 */
	void updateTotalSteps(int numSteps);

	/**
	 * Updates the total number of calories adding the number passed as
	 * parameter to the total
	 * 
	 * @pre numCalories > 0
	 * @param numCalories
	 */
	void updateTotalCalories(int numCalories);

	/**
	 * Adds a new athlete to the group.
	 * 
	 * @pre athlete != <b>null</b>
	 * @param athlete
	 */
	void addNewAthlete(AthleteGets athlete);

	/**
	 * Removes the given athlete from the group. Can only be called by the
	 * athlete that is going to be removed.
	 * 
	 * @pre athlete != <b>null</b>
	 * @param athlete
	 */
	void removeAthlete(AthleteGets athlete);

	/**
	 * Sets the group's previous total steps to the new total steps value
	 */
	void updateGroupPreviousTotalSteps();

	/**
	 * Sets the group's previous total calories to the new total calories value
	 */
	void updateGroupPreviousTotalCalories();

}
