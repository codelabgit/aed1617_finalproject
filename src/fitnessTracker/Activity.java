package fitnessTracker;

import java.io.Serializable;

/**
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public interface Activity extends Serializable {

	/**
	 * Returns the activity ID.
	 * 
	 * @return activity ID
	 */
	String getActivityID();

	/**
	 * Returns the activity name.
	 * 
	 * @return activity name
	 */
	String getActivityName();

	/**
	 * Returns the activity metabolic equivalent (MET).
	 * 
	 * @return metabolic equivalent
	 */
	int getMetabolicEquivalent();
}
