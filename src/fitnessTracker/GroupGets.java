package fitnessTracker;

import java.io.Serializable;

import dataStructures.Entry;
import dataStructures.Iterator;
import fitnessTracker.exceptions.GroupIsEmptyException;

/**
 * Non-changeable interface. Only for accessing the group's data. Can be used
 * outside its own package.
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public interface GroupGets extends Serializable {

	/**
	 * Returns the group ID.
	 * 
	 * @return group ID
	 */
	String getGroupID();

	/**
	 * Returns the group name.
	 * 
	 * @return group name
	 */
	String getGroupName();

	/**
	 * Returns the group total steps.
	 * 
	 * @return group total steps
	 */
	int getGroupTotalSteps();

	/**
	 * Returns the group total spent calories.
	 * 
	 * @return group spent calories
	 */
	int getGroupTotalSpentCalories();

	/**
	 * Returns the previous number of steps
	 * 
	 * @return previous number of steps
	 */
	int getGroupPreviousTotalSteps();

	/**
	 * Returns the previous number of calories spent
	 * 
	 * @return previous number of calories spent
	 */
	int getGroupPreviousTotalCalories();

	/**
	 * Returns the number of group members.
	 * 
	 * @return number of group members
	 */
	int getGroupMembersCount();

	/**
	 * Returns an iterator that lists the group athletes in alphabetical order.
	 * 
	 * @return athlete iterator
	 * @throws GroupIsEmptyException
	 */
	Iterator<Entry<String, AthleteGets>> getAthleteList() throws GroupIsEmptyException;

}
