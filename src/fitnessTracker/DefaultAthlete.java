package fitnessTracker;

import fitnessTracker.exceptions.AthleteAlreadyInGroupException;
import fitnessTracker.exceptions.AthleteNotInGroupException;

/**
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public class DefaultAthlete implements AthleteSets {

	private static final long serialVersionUID = 1L;

	private static final String FEMALE = "Feminino";
	private static final String MALE = "Masculino";
	private static final String TO_STRING_FORMAT = "%s: %s, %d kg, %d anos, %d cal, %d ps";

	private String name;
	private int weight, height, age;
	private char sex;
	private DeviceGets device;

	private GroupGets currentGroup;

	public DefaultAthlete(String trackerID, int weight, int height, int age, char sex, String name) {
		this.name = name;
		this.weight = weight;
		this.height = height;
		this.age = age;
		this.sex = sex;
		device = new DefaultDevice(trackerID, this);
		currentGroup = null;
	}

	@Override
	public DeviceGets getDevice() {
		return device;
	}

	@Override
	public String getAtheleteName() {
		return name;
	}

	@Override
	public int getAtheleteWeight() {
		return weight;
	}

	@Override
	public int getAtheleteHeight() {
		return height;
	}

	@Override
	public int getAtheleteAge() {
		return age;
	}

	@Override
	public char getAthleteSex() {
		return sex;
	}

	@Override
	public void editAthleteInfo(int weight, int height, int age) {
		this.weight = weight;
		this.height = height;
		this.age = age;
	}

	@Override
	public void enterGroup(GroupGets group) throws AthleteAlreadyInGroupException {
		if (currentGroup != null) {
			throw new AthleteAlreadyInGroupException();
		} else {
			currentGroup = group;
			((GroupSets) currentGroup).addNewAthlete(this);
		}
	}

	@Override
	public void quitGroup() throws AthleteNotInGroupException {
		if (currentGroup == null) {
			throw new AthleteNotInGroupException();
		} else {
			((GroupSets) currentGroup).removeAthlete(this);
			currentGroup = null;
		}
	}

	@Override
	public GroupGets getGroup() throws AthleteNotInGroupException {
		if (currentGroup == null) {
			throw new AthleteNotInGroupException();
		} else {
			return currentGroup;
		}
	}

	@Override
	public boolean hasGroup() {
		return currentGroup != null;
	}

	@Override
	public String getSexString() {
		if (getAthleteSex() == 'F') {
			return FEMALE;
		} else {
			return MALE;
		}
	}
	
	@Override
	public String athletesInGroupListFormat(){
		return String.format(TO_STRING_FORMAT, name, getSexString(), weight, age, device.getTotalCaloriesSpent(),
				device.getTotalSteps());
	}
	
	@Override
	public String toString() {
		if (currentGroup == null) {
			return String.format(TO_STRING_FORMAT, name, getSexString(), weight, age, device.getTotalCaloriesSpent(),
					device.getTotalSteps());
		} else {
			return String.format(TO_STRING_FORMAT + " (%s)", name, getSexString(), weight, age,
					device.getTotalCaloriesSpent(), device.getTotalSteps(), currentGroup.getGroupName());
		}

	}
}
