package fitnessTracker;

import java.io.Serializable;

/**
 * Interface used to access all the data of a training.
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */

public interface Training extends Serializable {

	/**
	 * Returns training information/details.
	 * 
	 * @return
	 */
	Activity getTrainingInformation();

	/**
	 * Returns used device.
	 * 
	 * @return used device
	 */
	DeviceGets getUsedDevice();

	/**
	 * Returns the training duration (in hours).
	 * 
	 * @return training duration
	 */
	int getTrainingDuration();

	/**
	 * Returns the calories spent during the training. Uses the Harris-Benedict
	 * equation to calculate spent calories. CS = (BMR / 24) * MET * T CS :
	 * Calories Spent BMR : Basal Metabolic Rate MET : Metabolic Equivalent T :
	 * Time (hours)
	 * 
	 * @return calories spent
	 */
	int getCaloriesSpent();
}
