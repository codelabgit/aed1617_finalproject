package fitnessTracker;

/**
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public class DefaultActivity implements Activity {

	private static final long serialVersionUID = 1L;
	
	private String acitvityID, name;
	private int MET;

	public DefaultActivity(String activityID, String name, int MET) {
		this.acitvityID = activityID;
		this.name = name;
		this.MET = MET;
	}

	@Override
	public String getActivityID() {
		return acitvityID;
	}

	@Override
	public String getActivityName() {
		return name;
	}

	@Override
	public int getMetabolicEquivalent() {
		return MET;
	}

}
