package fitnessTracker;

import java.io.Serializable;

import dataStructures.Iterator;
import fitnessTracker.exceptions.EmptyContestException;
import fitnessTracker.exceptions.InexistentGroupException;

/**
 * Interface that manages the contest, keeping track of the groups' stats to
 * display the walkers and warriors of the year.
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public interface Contest extends Serializable {

	/**
	 * Creates a new group with the given ID and name.
	 * 
	 * @param group
	 */
	void addGroup(GroupGets group);

	/**
	 * Returns an iterator for the group(s) with the most steps.
	 * 
	 * @return group with the most steps
	 */
	Iterator<GroupGets> getWalkersOfTheYear() throws EmptyContestException;

	/**
	 * Returns an iterator for the group(s) with the most spent calories.
	 * 
	 * @return group with the most spent calories
	 */
	Iterator<GroupGets> getWarriorsOfTheYear() throws EmptyContestException;

	/**
	 * Updates the state of the contest. Who's first, etc. Must be called
	 * whenever a member joins or exits the group.
	 * 
	 * @param lastUpdatedGroup
	 * @throws InexistentGroupException
	 * @throws EmptyContestException
	 */
	void updateContestStatus(GroupGets lastUpdatedGroup);
}
