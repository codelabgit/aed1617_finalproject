package fitnessTracker;

import fitnessTracker.exceptions.AthleteAlreadyInGroupException;
import fitnessTracker.exceptions.AthleteNotInGroupException;

/**
 * Changeable interface. Only for changing the athete's data. Can only be used
 * inside its own package.
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
interface AthleteSets extends AthleteGets {
	/**
	 * Edits the athlete's weight, height and age
	 * 
	 * @pre all params must be greater than zero
	 * @param weight
	 * @param height
	 * @param age
	 */
	void editAthleteInfo(int weight, int height, int age);

	/**
	 * The athlete enters a new group.
	 * 
	 * @pre Group exists
	 * @param group
	 * @throws AthleteAlreadyInGroupException
	 */
	void enterGroup(GroupGets group) throws AthleteAlreadyInGroupException;

	/**
	 * The athlete exits the current group he is in.
	 * 
	 * @throws AthleteNotInGroupException
	 * 
	 * @pre the athlete is in a group
	 */
	void quitGroup() throws AthleteNotInGroupException;

}
