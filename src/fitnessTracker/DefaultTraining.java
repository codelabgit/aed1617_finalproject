package fitnessTracker;

/**
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public class DefaultTraining implements Training {

	private static final long serialVersionUID = 1L;
	
	private static final String TO_STRING_FORMAT = "%s %d cal";
	private DeviceGets usedDevice;
	private Activity trainActivity;
	private int trainingDuration;
	private int caloriesSpent;

	public DefaultTraining(DeviceSets usedDevice, Activity trainActivity, int trainingDuration, int caloriesSpent) {
		this.usedDevice = usedDevice;
		this.trainActivity = trainActivity;
		this.trainingDuration = trainingDuration;
		this.caloriesSpent = caloriesSpent;
	}

	@Override
	public DeviceGets getUsedDevice() {
		return usedDevice;
	}

	@Override
	public Activity getTrainingInformation() {
		return trainActivity;
	}

	@Override
	public int getTrainingDuration() {
		return trainingDuration;
	}

	@Override
	public int getCaloriesSpent() {
		return caloriesSpent;
	}

	@Override
	public String toString() {
		return String.format(TO_STRING_FORMAT, getTrainingInformation().getActivityName(), getCaloriesSpent());
	}
}
