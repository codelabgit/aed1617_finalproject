package fitnessTracker;

import java.io.Serializable;

import dataStructures.Entry;
import dataStructures.Iterator;
import fitnessTracker.exceptions.AthleteAlreadyInGroupException;
import fitnessTracker.exceptions.AthleteNotInGroupException;
import fitnessTracker.exceptions.EmptyContestException;
import fitnessTracker.exceptions.ExistingActivityException;
import fitnessTracker.exceptions.ExistingAthleteException;
import fitnessTracker.exceptions.ExistingGroupException;
import fitnessTracker.exceptions.GroupIsEmptyException;
import fitnessTracker.exceptions.InexistentActivityException;
import fitnessTracker.exceptions.InexistentAthleteException;
import fitnessTracker.exceptions.InexistentAthleteTrainingsException;
import fitnessTracker.exceptions.InexistentGroupException;
import fitnessTracker.exceptions.InvalidListingTypeException;
import fitnessTracker.exceptions.InvalidSexException;

/**
 * Top-level interface that manages all the aspects of the system and brings
 * everything together.
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public interface FitnessTracker extends Serializable {

	/**
	 * Inserts a new athlete. Param 'sex' should be either "M" or "F".
	 * 
	 * @pre !hasAthlete
	 * @pre all numerical values must be positive
	 * @pre sex = 'M' or sex = 'F'
	 * @param trackerID
	 * @param weight
	 * @param height
	 * @param age
	 * @param sex
	 * @param name
	 * @throws ExistingAthleteException
	 * @throws InvalidSexException
	 */
	void insertAthlete(String trackerID, int weight, int height, int age, char sex, String name)
			throws ExistingAthleteException, InvalidSexException;

	/**
	 * Edits all the athlete's info except name and sex.
	 * 
	 * @pre hasAthlete
	 * @pre all numerical values must be positive
	 * @param trackerID
	 * @param weight
	 * @param height
	 * @param age
	 * @throws InexistentAthleteException
	 */
	void editAthleteInfo(String trackerID, int weight, int height, int age) throws InexistentAthleteException;

	/**
	 * Removes the athlete associated with the given trackerID.
	 * 
	 * @pre hasAthlete
	 * @param trackerID
	 * @throws InexistentAthleteException
	 * @throws EmptyContestException
	 * @throws InexistentGroupException
	 * @throws AthleteNotInGroupException
	 */
	void removeAthlete(String trackerID)
			throws InexistentAthleteException, AthleteNotInGroupException, InexistentGroupException;

	/**
	 * Returns the athlete with the given trackerID
	 * 
	 * @param trackerID
	 * @return athlete
	 */
	AthleteGets getAthlete(String trackerID) throws InexistentAthleteException;

	/**
	 * Creates a new Activity.
	 * 
	 * @pre MET > 0
	 * @pre !hasActivity
	 * @param activityID
	 * @param MET
	 * @param name
	 */
	void newActivity(String activityID, int MET, String name) throws ExistingActivityException;

	/**
	 * Returns the activity with the given activityID
	 * 
	 * @param activityID
	 * @return activity
	 */
	Activity getActivity(String activityID) throws InexistentActivityException;

	/**
	 * Creates a new Training.
	 * 
	 * @pre hasAthlete
	 * @pre hasActivity
	 * @pre duration > 0
	 * @param trackerID
	 * @param activityID
	 * @param duration
	 * @throws AthleteNotInGroupException
	 * @throws InexistentGroupException
	 * @throws EmptyContestException
	 */
	void newTraining(String trackerID, String activityID, int duration) throws InexistentActivityException,
			InexistentAthleteException, InexistentGroupException, AthleteNotInGroupException;

	/**
	 * Returns an iterator for a given athlete's trainings. Param type should be
	 * either 'T' to order the listing cronologically, starting at the most
	 * recent training, or 'C', to order it from the highest amount of calories
	 * burned to the lowest.
	 * 
	 * @pre hasAthlete
	 * @pre type = 'C' or type = 'T'
	 * @pre numTrainings > 0
	 * @param trackerID
	 * @param type
	 * @return training iterator
	 * @throws InvalidListingTypeException
	 * @throws InexistentAthleteException
	 * @throws InexistentAthleteTrainingsException
	 */
	Iterator<Training> listAthleteTrainings(String trackerID, char type)
			throws InvalidListingTypeException, InexistentAthleteException, InexistentAthleteTrainingsException;

	/**
	 * Updates the number of steps taken by a given athlete.
	 * 
	 * @pre hasAthlete
	 * @pre steps > 0
	 * @param trackerID
	 * @param steps
	 * @throws InexistentAthleteException
	 * @throws AthleteNotInGroupException
	 * @throws InexistentGroupException
	 * @throws EmptyContestException
	 */
	void updateSteps(String trackerID, int steps)
			throws InexistentAthleteException, InexistentGroupException, AthleteNotInGroupException;

	/**
	 * Creates a new athlete group.
	 * 
	 * @pre !hasGroup
	 * @param groupID
	 * @param name
	 * @throws ExistingGroupException
	 * @throws InexistentGroupException
	 * @throws EmptyContestException
	 */
	void newGroup(String groupID, String name) throws ExistingGroupException, InexistentGroupException;

	/**
	 * Returns the group with the given groupID
	 * 
	 * @param groupID
	 * @return group
	 */
	GroupGets getGroup(String groupID) throws InexistentGroupException;

	/**
	 * Joins a given athlete to a given group.
	 * 
	 * @pre hasAthlete
	 * @pre hasGroup
	 * @pre athlete is not in any group
	 * @param trackerID
	 * @param groupID
	 * @throws InexistentAthleteException
	 * @throws InexistentGroupException
	 * @throws AthleteAlreadyInGroupException
	 * @throws EmptyContestException
	 */
	void joinGroup(String trackerID, String groupID)
			throws InexistentAthleteException, AthleteAlreadyInGroupException, InexistentGroupException;

	/**
	 * Removes a given athlete from a given group.
	 * 
	 * @pre hasAthlete
	 * @pre hasGroup
	 * @pre athlete is in the given group
	 * @param trackerID
	 * @param groupID
	 * @throws InexistentGroupException
	 * @throws InexistentAthleteException
	 * @throws AthleteNotInGroupException
	 * @throws EmptyContestException
	 */
	void quitGroup(String trackerID, String groupID)
			throws InexistentAthleteException, InexistentGroupException, AthleteNotInGroupException;

	/**
	 * Returns a String containing the group's stats (steps taken and calories
	 * burned).
	 * 
	 * @pre hasGroup
	 * @param groupID
	 * @return String with group's stats
	 * @throws InexistentGroupException
	 */
	GroupGets consultGroupStats(String groupID) throws InexistentGroupException;

	/**
	 * Returns an iterator to list all the athletes in a given group.
	 * 
	 * @pre hasGroup
	 * @param groupID
	 * @return athlete iterator
	 * @throws InexistentGroupException
	 * @throws GroupIsEmptyException
	 */
	Iterator<Entry<String, AthleteGets>> listAthletesInGroup(String groupID)
			throws InexistentGroupException, GroupIsEmptyException;

	/**
	 * Lists the current ratings for the groups in the Walkers Of The Year
	 * contest, ordering them from the highest-rated group to the lowest.
	 * 
	 * @pre numGroups > 0
	 * @return group iterator
	 * @throws EmptyContestException
	 */
	Iterator<GroupGets> listGroupsWalkersContest() throws EmptyContestException;

	/**
	 * Lists the current ratings for the groups in the Warriors Of The Year
	 * contest, ordering them from the highest-rated group to the lowest.
	 * 
	 * @pre numGroups > 0
	 * @return group iterator
	 * @throws EmptyContestException
	 */
	Iterator<GroupGets> listGroupsWarriorsContest() throws EmptyContestException;

}
