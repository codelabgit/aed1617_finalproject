package fitnessTracker;

import java.io.Serializable;

import dataStructures.Iterator;
import fitnessTracker.exceptions.InexistentAthleteTrainingsException;

/**
 * Non-changeable interface. Only for accessing the device's data. Can be used
 * outside its own package.
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public interface DeviceGets extends Serializable {

	/**
	 * Returns the device ID.
	 * 
	 * @return device ID
	 */
	String getDeviceID();

	/**
	 * Returns the number of total steps.
	 * 
	 * @return total steps
	 */
	int getTotalSteps();

	/**
	 * Returns the total of calories spent.
	 * 
	 * @return total calories spent
	 */
	int getTotalCaloriesSpent();

	/**
	 * Returns an iterator to list all the trainings by calories
	 * 
	 * @return trainings iterator by calories
	 * @throws InexistentAthleteTrainingsException
	 */
	Iterator<Training> listTrainingsByCalories() throws InexistentAthleteTrainingsException;

	/**
	 * Returns an iterator to list all the trainings by time
	 * 
	 * @return trainings iterator by time
	 * @throws InexistentAthleteTrainingsException
	 */
	Iterator<Training> listTrainingsByTime() throws InexistentAthleteTrainingsException;

}
