package fitnessTracker;

import dataStructures.ChainedHashTable;
import dataStructures.Dictionary;
import dataStructures.Entry;
import dataStructures.Iterator;
import fitnessTracker.exceptions.AthleteAlreadyInGroupException;
import fitnessTracker.exceptions.AthleteNotInGroupException;
import fitnessTracker.exceptions.EmptyContestException;
import fitnessTracker.exceptions.ExistingActivityException;
import fitnessTracker.exceptions.ExistingAthleteException;
import fitnessTracker.exceptions.ExistingGroupException;
import fitnessTracker.exceptions.GroupIsEmptyException;
import fitnessTracker.exceptions.InexistentActivityException;
import fitnessTracker.exceptions.InexistentAthleteException;
import fitnessTracker.exceptions.InexistentAthleteTrainingsException;
import fitnessTracker.exceptions.InexistentGroupException;
import fitnessTracker.exceptions.InvalidListingTypeException;
import fitnessTracker.exceptions.InvalidSexException;

/**
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public class DefaultFitnessTracker implements FitnessTracker {

	private static final long serialVersionUID = 1L;

	private static final char MALE = 'M';
	private static final char FEMALE = 'F';
	private static final char SORT_BY_CALORIES = 'C';
	private static final char SORT_BY_TIME = 'T';
	private static final int MIN_VALUE = 0;

	private Dictionary<String, AthleteGets> athletes;
	private Dictionary<String, Activity> activities;
	private Dictionary<String, GroupGets> groups;
	private DefaultContest contest;

	public DefaultFitnessTracker() {
		athletes = new ChainedHashTable<String, AthleteGets>();
		activities = new ChainedHashTable<String, Activity>();
		groups = new ChainedHashTable<String, GroupGets>();
		contest = new DefaultContest();
	}

	@Override
	public void insertAthlete(String trackerID, int weight, int height, int age, char sex, String name)
			throws ExistingAthleteException, InvalidSexException {
		if (sex != MALE && sex != FEMALE) {
			throw new InvalidSexException();
		} else if (weight < MIN_VALUE || height < MIN_VALUE || age < MIN_VALUE) {
			throw new NumberFormatException();
		} else if (hasAthlete(trackerID)) {
			throw new ExistingAthleteException();
		} else {
			athletes.insert(trackerID, new DefaultAthlete(trackerID, weight, height, age, sex, name));
		}

	}

	@Override
	public void editAthleteInfo(String trackerID, int weight, int height, int age) throws InexistentAthleteException {
		if (!hasAthlete(trackerID)) {
			throw new InexistentAthleteException();
		} else if (weight < MIN_VALUE || height < MIN_VALUE || age < MIN_VALUE) {
			throw new NumberFormatException();
		} else {
			AthleteSets athlete = (AthleteSets) getAthlete(trackerID);
			athlete.editAthleteInfo(weight, height, age);
		}
	}

	@Override
	public void removeAthlete(String trackerID)
			throws InexistentAthleteException, AthleteNotInGroupException, InexistentGroupException {
		if (!hasAthlete(trackerID)) {
			throw new InexistentAthleteException();
		} else {
			AthleteGets removedAthlete = athletes.remove(trackerID);

			if (removedAthlete.hasGroup()) {
				GroupSets group = (GroupSets) removedAthlete.getGroup();
				group.removeAthlete(removedAthlete);
				contest.updateContestStatus(group);
			}
		}
	}

	@Override
	public AthleteGets getAthlete(String trackerID) throws InexistentAthleteException {
		if (!hasAthlete(trackerID)) {
			throw new InexistentAthleteException();
		} else {
			return athletes.find(trackerID);
		}
	}

	@Override
	public void newActivity(String activityID, int MET, String name) throws ExistingActivityException {
		if (MET >= MIN_VALUE) {
			if (hasActivity(activityID)) {
				throw new ExistingActivityException();
			} else {
				activities.insert(activityID, new DefaultActivity(activityID, name, MET));
			}
		} else {
			throw new NumberFormatException();
		}
	}

	@Override
	public Activity getActivity(String activityID) throws InexistentActivityException {
		if (!hasActivity(activityID))
			throw new InexistentActivityException();
		else
			return activities.find(activityID);
	}

	@Override
	public void newTraining(String trackerID, String activityID, int duration) throws InexistentActivityException,
			InexistentAthleteException, InexistentGroupException, AthleteNotInGroupException {
		if (duration < MIN_VALUE) {
			throw new NumberFormatException();
		} else {
			if (!hasAthlete(trackerID)) {
				throw new InexistentAthleteException();
			} else if (!hasActivity(activityID)) {
				throw new InexistentActivityException();
			} else {
				DeviceSets device = (DeviceSets) getAthlete(trackerID).getDevice();
				device.newTraining(getActivity(activityID), duration);

				if (getAthlete(trackerID).hasGroup()){
					contest.updateContestStatus(getAthlete(trackerID).getGroup());
				}

			}
		}
	}

	@Override
	public Iterator<Training> listAthleteTrainings(String trackerID, char type)
			throws InvalidListingTypeException, InexistentAthleteException, InexistentAthleteTrainingsException {
		if (type != SORT_BY_TIME && type != SORT_BY_CALORIES) {
			throw new InvalidListingTypeException();
		} else if (!hasAthlete(trackerID)) {
			throw new InexistentAthleteException();
		} else {
			DeviceGets device = getAthlete(trackerID).getDevice();

			switch (type) {
			case SORT_BY_TIME:
				return device.listTrainingsByTime();

			case SORT_BY_CALORIES:
				return device.listTrainingsByCalories();

			default:
				return null;
			}
		}
	}

	@Override
	public void updateSteps(String trackerID, int steps) throws InexistentAthleteException, AthleteNotInGroupException {
		if (steps < MIN_VALUE) {
			throw new NumberFormatException();
		} else if (!hasAthlete(trackerID)) {
			throw new InexistentAthleteException();
		} else {
			DeviceSets device = (DeviceSets) getAthlete(trackerID).getDevice();
			device.updateSteps(steps);

			if (getAthlete(trackerID).hasGroup()) {
				contest.updateContestStatus(getAthlete(trackerID).getGroup());
			}
		}

	}

	@Override
	public void newGroup(String groupID, String name) throws ExistingGroupException, InexistentGroupException {
		GroupSets group = new DefaultGroup(groupID, name);
		if (hasGroup(groupID)) {
			throw new ExistingGroupException();
		} else {
			groups.insert(groupID, group);
			contest.addGroup(group);
		}
	}

	@Override
	public GroupGets getGroup(String groupID) throws InexistentGroupException {
		if (!hasGroup(groupID))
			throw new InexistentGroupException();
		else
			return groups.find(groupID);
	}

	@Override
	public void joinGroup(String trackerID, String groupID)
			throws InexistentAthleteException, AthleteAlreadyInGroupException, InexistentGroupException {
		if (!hasAthlete(trackerID)) {
			throw new InexistentAthleteException();
		} else if (!hasGroup(groupID)) {
			throw new InexistentGroupException();
		} else {
			AthleteSets athlete = (AthleteSets) getAthlete(trackerID);
			athlete.enterGroup(getGroup(groupID));
			contest.updateContestStatus(getGroup(groupID));
		}

	}

	@Override
	public void quitGroup(String trackerID, String groupID)
			throws InexistentAthleteException, InexistentGroupException, AthleteNotInGroupException {
		if (!hasAthlete(trackerID)) {
			throw new InexistentAthleteException();
		} else if (!hasGroup(groupID)) {
			throw new InexistentGroupException();
		} else {
			AthleteSets athlete = (AthleteSets) getAthlete(trackerID);
			athlete.quitGroup();
			contest.updateContestStatus(getGroup(groupID));
		}
	}

	@Override
	public GroupGets consultGroupStats(String groupID) throws InexistentGroupException {
		if (!hasGroup(groupID)) {
			throw new InexistentGroupException();
		} else {
			return getGroup(groupID);
		}
	}

	@Override
	public Iterator<Entry<String, AthleteGets>> listAthletesInGroup(String groupID)
			throws InexistentGroupException, GroupIsEmptyException {
		if (!hasGroup(groupID)) {
			throw new InexistentGroupException();
		} else {
			return getGroup(groupID).getAthleteList();
		}
	}

	@Override
	public Iterator<GroupGets> listGroupsWalkersContest() throws EmptyContestException {
		if (groups == null) {
			throw new EmptyContestException();
		} else {
			return contest.getWalkersOfTheYear();
		}
	}

	@Override
	public Iterator<GroupGets> listGroupsWarriorsContest() throws EmptyContestException {
		if (groups == null) {
			throw new EmptyContestException();
		} else {
			return contest.getWarriorsOfTheYear();
		}
	}

	private boolean hasAthlete(String trackerID) {
		return athletes.find(trackerID) != null;
	}

	private boolean hasActivity(String activityID) {
		return activities.find(activityID) != null;
	}

	private boolean hasGroup(String groupID) {
		return groups.find(groupID) != null;
	}

}
