package fitnessTracker;

import dataStructures.AVLTree;
import dataStructures.Entry;
import dataStructures.Iterator;
import dataStructures.OrderedDictionary;
import fitnessTracker.exceptions.GroupIsEmptyException;

/**
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public class DefaultGroup implements GroupSets {

	private static final long serialVersionUID = 1L;

	private static final String TO_STRING_FORMAT = "Grupo %s: %d cal, %d ps";
	private static final int INT_DECLARING_VALUE = 0;

	private OrderedDictionary<String, AthleteGets> groupAthletes;

	private String groupID;
	private String groupName;
	private int groupPreviousTotalSteps;
	private int groupTotalSteps;
	private int groupPreviousTotalSpentCalories;
	private int groupTotalSpentCalories;

	public DefaultGroup(String groupID, String groupName) {
		this.groupID = groupID;
		this.groupName = groupName;
		groupTotalSteps = INT_DECLARING_VALUE;
		groupTotalSpentCalories = INT_DECLARING_VALUE;
		groupPreviousTotalSpentCalories = INT_DECLARING_VALUE;
		groupPreviousTotalSteps = INT_DECLARING_VALUE;
		groupAthletes = new AVLTree<String, AthleteGets>();
	}

	@Override
	public String getGroupID() {
		return groupID;
	}

	@Override
	public String getGroupName() {
		return groupName;
	}

	@Override
	public void updateTotalSteps(int numSteps) {
		groupPreviousTotalSteps = groupTotalSteps;
		groupTotalSteps += numSteps;
	}

	@Override
	public void updateTotalCalories(int numCalories) {
		groupPreviousTotalSpentCalories = groupTotalSpentCalories;
		groupTotalSpentCalories += numCalories;
	}

	@Override
	public int getGroupTotalSteps() {
		return groupTotalSteps;
	}

	@Override
	public int getGroupTotalSpentCalories() {
		return groupTotalSpentCalories;
	}

	@Override
	public int getGroupMembersCount() {
		return groupAthletes.size();
	}

	@Override
	public void addNewAthlete(AthleteGets athlete) {
		groupAthletes.insert(athlete.getAtheleteName(), athlete);
		
		groupTotalSteps += athlete.getDevice().getTotalSteps();
		groupTotalSpentCalories += athlete.getDevice().getTotalCaloriesSpent();
	}

	@Override
	public void removeAthlete(AthleteGets athlete) {
		groupAthletes.remove(athlete.getAtheleteName());
		
		groupTotalSteps -= athlete.getDevice().getTotalSteps();
		groupTotalSpentCalories -= athlete.getDevice().getTotalCaloriesSpent();
	}

	@Override
	public Iterator<Entry<String, AthleteGets>> getAthleteList() throws GroupIsEmptyException {
		if (groupAthletes.isEmpty()) {
			throw new GroupIsEmptyException();
		} else {
			return groupAthletes.iterator();
		}
	}

	@Override
	public String toString() {
		return String.format(TO_STRING_FORMAT, getGroupName(), getGroupTotalSpentCalories(), getGroupTotalSteps());
	}

	@Override
	public int getGroupPreviousTotalSteps() {
		return groupPreviousTotalSpentCalories;
	}

	@Override
	public int getGroupPreviousTotalCalories() {
		return groupPreviousTotalSteps;
	}
	
	@Override
	public void updateGroupPreviousTotalSteps(){
		groupPreviousTotalSteps = groupTotalSteps;
	}
	
	@Override
	public void updateGroupPreviousTotalCalories(){
		groupPreviousTotalSpentCalories = groupTotalSpentCalories;
	}
}
