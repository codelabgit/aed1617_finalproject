package fitnessTracker;

import dataStructures.AVLTree;
import dataStructures.BSTKeyOrderIterator;
import dataStructures.BSTWithStackIterator;
import dataStructures.IterableStack;
import dataStructures.IterableStackInList;
import dataStructures.Iterator;
import dataStructures.OrderedDictionary;
import fitnessTracker.exceptions.AthleteNotInGroupException;
import fitnessTracker.exceptions.InexistentAthleteTrainingsException;

/**
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public class DefaultDevice implements DeviceSets {

	private static final long serialVersionUID = 1L;

	private static final int DEFAULT_VALUE = 0;

	private String deviceID;
	private IterableStack<Training> trainingsByTime;
	private OrderedDictionary<Integer, IterableStack<Training>> trainingsByCalories;
	private int numSteps, totalCalories;
	private AthleteGets owner;

	public DefaultDevice(String deviceID, AthleteGets owner) {
		this.deviceID = deviceID;
		this.owner = owner;
		numSteps = DEFAULT_VALUE;
		totalCalories = DEFAULT_VALUE;
		trainingsByCalories = new AVLTree<Integer, IterableStack<Training>>();
		trainingsByTime = new IterableStackInList<Training>();
	}

	@Override
	public String getDeviceID() {
		return deviceID;
	}

	@Override
	public int getTotalSteps() {
		return numSteps;
	}

	@Override
	public void updateSteps(int steps) throws AthleteNotInGroupException {
		numSteps += steps;

		if (owner.hasGroup()) {
			GroupSets group = (GroupSets) owner.getGroup();
			group.updateTotalSteps(steps);
		}
	}

	@Override
	public int getTotalCaloriesSpent() {
		return totalCalories;
	}

	@Override
	public void newTraining(Activity activity, int duration) throws AthleteNotInGroupException {
		int spentCalories = Calories.calculateCalories(owner.getAtheleteWeight(), owner.getAtheleteHeight(),
				owner.getAthleteSex(), owner.getAtheleteAge(), activity.getMetabolicEquivalent(), duration);

		Training newTraining = new DefaultTraining(this, activity, duration, spentCalories);

		trainingsByTime.push(newTraining);

		IterableStack<Training> tempStack = trainingsByCalories.find(-spentCalories);

		if (tempStack != null) {
			tempStack.push(newTraining);
		} else {
			tempStack = new IterableStackInList<Training>();
			tempStack.push(newTraining);
			trainingsByCalories.insert(-spentCalories, tempStack);
		}
		totalCalories += spentCalories;

		if (owner.hasGroup()) {
			GroupSets group = (GroupSets) owner.getGroup();
			group.updateTotalCalories(spentCalories);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public Iterator<Training> listTrainingsByCalories() throws InexistentAthleteTrainingsException {
		if (trainingsByCalories.isEmpty())
			throw new InexistentAthleteTrainingsException();

		BSTKeyOrderIterator<Integer, IterableStack<Training>> treeIterator = 
				(BSTKeyOrderIterator<Integer, IterableStack<Training>>) trainingsByCalories.iterator();

		return new BSTWithStackIterator<Integer, Training>(treeIterator);
	}

	@Override
	public Iterator<Training> listTrainingsByTime() throws InexistentAthleteTrainingsException {
		if (trainingsByTime.isEmpty())
			throw new InexistentAthleteTrainingsException();

		return trainingsByTime.iterator();
	}

}
