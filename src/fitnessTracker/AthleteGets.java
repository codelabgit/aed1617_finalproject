package fitnessTracker;

/**
 * Non-changeable interface. Only for accessing the Athlete's data. Can be used outside its own package.
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
import java.io.Serializable;

import fitnessTracker.exceptions.AthleteNotInGroupException;

public interface AthleteGets extends Serializable {

	/**
	 * Returns the athlete's device.
	 * 
	 * @return string of characters identifying the athlete
	 */
	DeviceGets getDevice();

	/**
	 * Returns the athlete name.
	 * 
	 * @return athlete name
	 */
	String getAtheleteName();

	/**
	 * Returns the athlete weight, measured in grams.
	 * 
	 * @return athlete weight
	 */
	int getAtheleteWeight();

	/**
	 * Returns the athlete height, measured in centimeters.
	 * 
	 * @return athlete height
	 */
	int getAtheleteHeight();

	/**
	 * Returns the athlete age.
	 * 
	 * @return athlete age
	 */
	int getAtheleteAge();

	/**
	 * Returns the athlete's sex. The return val can only be either 'M' or 'F'
	 * 
	 * @return athlete's sex
	 */
	char getAthleteSex();

	/**
	 * Returns the athlete's sex in String format. The return val can only be
	 * "Masculino" or "Feminino"
	 * 
	 * @return athlete's sex in String format
	 */
	String getSexString();

	/**
	 * Returns the group the athlete is in.
	 * 
	 * @return group the athlete is in
	 * @throws AthleteNotInGroupException
	 */
	GroupGets getGroup() throws AthleteNotInGroupException;

	/**
	 * Checks if the athlete is in a group
	 * 
	 * @return true if he is, false otherwise
	 */
	boolean hasGroup();

	/**
	 * Returns a String in the display format for when listing athletes in a
	 * group
	 * 
	 * @return string in the display format
	 */
	String athletesInGroupListFormat();

}
