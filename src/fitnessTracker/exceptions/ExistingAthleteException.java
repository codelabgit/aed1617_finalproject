package fitnessTracker.exceptions;

/**
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public class ExistingAthleteException extends Exception {

	private static final long serialVersionUID = 1L;

	public ExistingAthleteException() {
		super();
	}

	public ExistingAthleteException(String message) {
		super(message);
	}

}
