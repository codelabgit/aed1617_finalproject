package fitnessTracker.exceptions;

/**
 * 
 * @author Andre Rodrigues 47806 ao.rodrigues@campus.fct.unl.pt
 * @author Jose Duarte 47288 jmg.duarte@campus.fct.unl.pt
 *
 */
public class InexistentAthleteTrainingsException extends Exception {

	private static final long serialVersionUID = 1L;

	public InexistentAthleteTrainingsException() {
		super();
	}

	public InexistentAthleteTrainingsException(String message) {
		super(message);
	}

}
